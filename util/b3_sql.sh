srvUSR=root
srvPWD=Inc0rrect
srvIP=127.0.0.1
panelIP=mysql.iareserver.com
remoteUSR=agent

mysqlID=${MYSQLID}
homeID=${PWD##*/}
srvID=b3_${homeID}
dbPass=$(</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8)

	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "CREATE DATABASE IF NOT EXISTS ${srvID}"
	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "GRANT ALL ON ${srvID}.* TO '${srvID}'@'%' IDENTIFIED BY '${dbPass}'"
	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "FLUSH PRIVILEGES;"
	mysql -u$srvUSR -p$srvPWD -h$srvIP -D ${srvID} < b3/sql/b3.sql

	
	mysql -u$remoteUSR -p$srvPWD -h$panelIP -e "INSERT INTO ogp_website.ogp_mysql_databases(mysql_server_id, home_id, db_user, db_passwd, db_name, enabled) VALUES (${mysqlID},${homeID},'${srvID}','${dbPass}','${srvID}',1)"
