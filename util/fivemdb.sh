srvUSR=userdb
srvPWD=passdb
srvIP=IPdb


mysqlID=4;
homeID=${PWD##*/}
srvID=fivem_${homeID}
dbPass=$(</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8)


count=`mysql -u$srvUSR -p$srvPWD -h$srvIP -s -N -e "SELECT count(User) FROM mysql.user WHERE User='$srvID'"`
count=$(($count + 0))
if [[ $count < 1 ]]; then
	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "CREATE DATABASE IF NOT EXISTS ${srvID}"
	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "GRANT ALL ON ${srvID}.* TO '${srvID}'@'%' IDENTIFIED BY '${dbPass}'"
	mysql -u$srvUSR -p$srvPWD -h$srvIP -e "FLUSH PRIVILEGES;"
	
	id=`mysql -u$srvUSR -p$srvPWD -h$srvIP -s -N -e "SELECT home_id FROM ogp_novo.ogp_mysql_databases WHERE home_id='$homeID'"`
	id=$(($id + 0))
	if [[ $id < 1 ]]; then
		mysql -u$srvUSR -p$srvPWD -h$srvIP -e "INSERT INTO ogp_novo.ogp_mysql_databases(mysql_server_id, home_id, db_user, db_passwd, db_name, enabled) VALUES (${mysqlID},${homeID},'${srvID}','${dbPass}','${srvID}',1)"
	fi
fi