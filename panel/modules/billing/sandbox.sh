#!/bin/bash

if [ "$1" == "enable" ];then
	sed -i "s/public $use_sandbox = .*/public $use_sandbox = true;/g" ipnlistener.php
	sed -i "s/$listener->use_sandbox = .*/$listener->use_sandbox = true;/g" paid-ipn.php
	sed -i "s/$p->paypal_url = .*/$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';/g" paypal.php
else
	sed -i "s/public $use_sandbox = .*/public $use_sandbox = false;/g" ipnlistener.php
	sed -i "s/$listener->use_sandbox = .*/$listener->use_sandbox = false;/g" ipnlistener.php
	sed -i "s/$p->paypal_url = .*/$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';/g" paypal.php
fi
