<?php exit;?>

[website]
date_format = "d/M/Y"
results_per_page = "10"
image_quality = "90"
max_image_width = "960"
enable_search = "0"
WYSIWYG = "TinyMCE"
tinymce_lang = "en_GB"
tinymce_skin = "lightgray"
gallery_theme = "dark_rounded"
images_bottom = "0"
safe_HTML = "0"

