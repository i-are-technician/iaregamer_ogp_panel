<?php /* Smarty version 2.6.30, created on 2019-08-23 07:14:53
         compiled from vServerLiveview.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'vServerLiveview.tpl', 27, false),)), $this); ?>
<div>
	<a href="#" id="clearLink" style=""></a>
	<img src="modules/TS3Admin/images/loading.gif" id="loadingImage" style="visibility:hidden;" />
	<img src="modules/TS3Admin/images/spacer.png" width="16" height="16" style="visibility:hidden;" />
	<a href="home.php?m=TS3Admin"><?php echo $this->_tpl_vars['lang']['OGP_LANG_head_vserver_overview']; ?>
</a> | <a href="home.php?m=TS3Admin&token"><?php echo $this->_tpl_vars['lang']['OGP_LANG_head_vserver_token']; ?>
</a>
</div>

<span style="float:left;">
	<fieldset class="infoBox" style="width:320px;"><!--float:left;--><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_server_virtualserver']; ?>
 #<?php echo $this->_tpl_vars['data']['virtualserver_id']; ?>
 <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_server_head']; ?>
</legend>
		<!--not implemented ;) but backup works!-->
		<div id="serverview">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'liveview.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<input type="hidden" name="liveViewSelection" id="liveViewSelection" />
		<br />
		<input type="checkbox" name="liveViewAutoUpdateActivated" id="liveViewAutoUpdateActivated" /> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_liveview_enable_autorefresh']; ?>

		&nbsp;&nbsp;&nbsp;<a href="javascript:serverViewUpdate(true);document.getElementById('clearLink').focus();"><img src="modules/TS3Admin/images/refresh.png" alt="reload" border="0" /></a>
	</fieldset>
	<fieldset style="width:320px;"><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_channelbackup_head']; ?>
</legend>
		<p>
			<form action="home.php?m=TS3Admin&getchannelbackup&type=cleared" method="post" target="_blank">
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_channelbackup_get']; ?>
" />
			</form>
		</p><br />
		<hr />
	<?php if (! empty ( $this->_tpl_vars['insertResult'] )): ?>
		<?php if (((is_array($_tmp=$this->_tpl_vars['insertResult'][0])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)) == 'ERROR'): ?>
			<p><?php echo $this->_tpl_vars['lang']['OGP_LANG_error']; ?>
 <?php echo $this->_tpl_vars['insertResult'][1]; ?>
: <?php echo $this->_tpl_vars['insertResult'][2]; ?>
</p>
		<?php else: ?>
			<p>
				<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_channelbackup_new_added_ok']; ?>

			</p>
		<?php endif; ?>
	<?php endif; ?>
		<p>
			<h3><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_channelbackup_load']; ?>
</h3>
			<form action="home.php?m=TS3Admin&liveview&do=loadbackup" method="post" enctype="multipart/form-data">
				<input type="file" name="backup" accept="text/plain" style="width:200px;" /><br /><br />
				<input type="submit" name="loadChannelBackupSubmit" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsliveview_channelbackup_load_submit']; ?>
" />
			</form>
		</p>
	</fieldset>
</span>


<fieldset class="infoBox" style="width:700px;"><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_head']; ?>
</legend>
	<div id="banlist">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'banlist.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
	<br /><a href="javascript:banListUpdate();document.getElementById('clearLink').focus();"><img src="modules/TS3Admin/images/refresh.png" alt="reload" border="0" /></a>
</fieldset>
<fieldset class="infoBox" style="width:310px;"><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_new_head']; ?>
</legend>
	<table>
		<tr>
			<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_ip']; ?>
</b></td>
			<td><input type="text" name="newBanIP" id="newBanIP" value="" style="width:140px;" /></td>
		</tr>
		<tr>
			<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_name']; ?>
</b></td>
			<td><input type="text" name="newBanName" id="newBanName" value="" style="width:140px;" /></td>
		</tr>
		<tr>
			<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_uid']; ?>
</b></td>
			<td><input type="text" name="newBanUID" id="newBanUID" value="" style="width:200px;" /></td>
		</tr>
		<tr>
			<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_reason']; ?>
</b></td>
			<td><input type="text" name="newBanReason" id="newBanReason" value="" style="width:200px;" /></td>
		</tr>
		<tr>
			<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_duration']; ?>
</b></td>
			<td>
				<input type="text" name="newBanDuration" id="newBanDuration" value="" style="width:50px;" />&nbsp;
				<select name="newBanDurationMode" id="newBanDurationMode" onchange="if(this.selectedIndex==0) document.getElementById('newBanDuration').disabled=true;else document.getElementById('newBanDuration').disabled=false;">
					<option value="0"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_unlimited']; ?>
</option>
					<option value="1"><?php echo $this->_tpl_vars['lang']['OGP_LANG_time_seconds']; ?>
</option>
					<option value="60" selected="selected"><?php echo $this->_tpl_vars['lang']['OGP_LANG_time_minutes']; ?>
</option>
					<option value="3600"><?php echo $this->_tpl_vars['lang']['OGP_LANG_time_hours']; ?>
</option>
					<option value="86400"><?php echo $this->_tpl_vars['lang']['OGP_LANG_time_days']; ?>
</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="button" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_new_create']; ?>
" onclick="addBan();" /></td>
		</tr>
	</table>
</fieldset>
<br class="clear" />