<?php /* Smarty version 2.6.30, created on 2019-08-23 06:52:59
         compiled from selectvServer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'selectvServer.tpl', 17, false),)), $this); ?>
<div>
	<a href="#" id="clearLink" style=""></a>
	<img src="modules/TS3Admin/images/loading.gif" id="loadingImage" style="visibility:hidden;" />
</div>		
<form action="home.php?m=TS3Admin" method="post">
	<table border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_id']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_name']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_ip']; ?>
:<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_port']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_state']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_clients']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_uptime']; ?>
</b></td>
		</tr>
<?php $_from = $this->_tpl_vars['selectvServer']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['curvServer']):
?>
		<tr class="<?php echo smarty_function_cycle(array('values' => "table2,table1"), $this);?>
">
			<td><input type="radio" name="vserver" value="<?php echo $this->_tpl_vars['curvServer']['virtualserver_id']; ?>
" /></td>
			<td><?php echo $this->_tpl_vars['curvServer']['virtualserver_id']; ?>
</td>
			<td><?php echo $this->_tpl_vars['curvServer']['virtualserver_name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['display_public_ip']; ?>
:<?php echo $this->_tpl_vars['curvServer']['virtualserver_port']; ?>
</td>
			<td><span id="serverstatus<?php echo $this->_tpl_vars['curvServer']['virtualserver_id']; ?>
" class="<?php if ($this->_tpl_vars['curvServer']['virtualserver_status'] == 'none'): ?>offline<?php else: ?><?php echo $this->_tpl_vars['curvServer']['virtualserver_status']; ?>
<?php endif; ?>"><?php if ($this->_tpl_vars['curvServer']['virtualserver_status'] == 'none'): ?>offline<?php else: ?><?php echo $this->_tpl_vars['curvServer']['virtualserver_status']; ?>
<?php endif; ?></span></td>
			<td><?php echo $this->_tpl_vars['curvServer']['virtualserver_clientsonline']; ?>
/<?php echo $this->_tpl_vars['curvServer']['virtualserver_maxclients']; ?>
</td>
			<td><!--'.TS3webinterface::parseTime($data[$i]['virtualserver_uptime']).'--><!--<?php echo $this->_tpl_vars['curvServer']['virtualserver_uptime']; ?>
--><?php echo $this->_tpl_vars['webinterface']->parseTime($this->_tpl_vars['curvServer']['virtualserver_uptime']); ?>
</td>
		</tr>
<?php endforeach; endif; unset($_from); ?>
	</table>
	<br /><br />
	<span style="border-top:1px solid #CCCCCC;padding-top:13px;">
		<input type="submit" name="vserverSubmit" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_choose']; ?>
" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="startvServer" onclick="setvserverstate('START');" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_start']; ?>
" />&nbsp;&nbsp;<input type="button" name="stopvServer" onclick="setvserverstate('STOP');" value="<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_stop']; ?>
" />
	</span>
</form>
<br />