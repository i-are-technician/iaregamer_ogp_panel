<?php /* Smarty version 2.6.30, created on 2019-08-23 07:14:53
         compiled from banlist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'banlist.tpl', 15, false),array('modifier', 'replace', 'banlist.tpl', 17, false),)), $this); ?>
	<table width="100%" border="0" cellspacing="1" cellpadding="3">
		<tr>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_id']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_ip']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_name']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_uid']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_reason']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_created']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_duration']; ?>
</b></td>
			<td class="table0"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_end']; ?>
</b></td>
			<td class="table0">&nbsp;</td>
		</tr>
<?php if (! empty ( $this->_tpl_vars['banList'][0]['banid'] )): ?>
<?php unset($this->_sections['ban']);
$this->_sections['ban']['loop'] = is_array($_loop=$this->_tpl_vars['banList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ban']['name'] = 'ban';
$this->_sections['ban']['show'] = true;
$this->_sections['ban']['max'] = $this->_sections['ban']['loop'];
$this->_sections['ban']['step'] = 1;
$this->_sections['ban']['start'] = $this->_sections['ban']['step'] > 0 ? 0 : $this->_sections['ban']['loop']-1;
if ($this->_sections['ban']['show']) {
    $this->_sections['ban']['total'] = $this->_sections['ban']['loop'];
    if ($this->_sections['ban']['total'] == 0)
        $this->_sections['ban']['show'] = false;
} else
    $this->_sections['ban']['total'] = 0;
if ($this->_sections['ban']['show']):

            for ($this->_sections['ban']['index'] = $this->_sections['ban']['start'], $this->_sections['ban']['iteration'] = 1;
                 $this->_sections['ban']['iteration'] <= $this->_sections['ban']['total'];
                 $this->_sections['ban']['index'] += $this->_sections['ban']['step'], $this->_sections['ban']['iteration']++):
$this->_sections['ban']['rownum'] = $this->_sections['ban']['iteration'];
$this->_sections['ban']['index_prev'] = $this->_sections['ban']['index'] - $this->_sections['ban']['step'];
$this->_sections['ban']['index_next'] = $this->_sections['ban']['index'] + $this->_sections['ban']['step'];
$this->_sections['ban']['first']      = ($this->_sections['ban']['iteration'] == 1);
$this->_sections['ban']['last']       = ($this->_sections['ban']['iteration'] == $this->_sections['ban']['total']);
?>
		<tr id="banRow_<?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['banid']; ?>
" class="<?php echo smarty_function_cycle(array('values' => "table2,table1"), $this);?>
">
			<td><?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['banid']; ?>
</td>
			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['banList'][$this->_sections['ban']['index']]['ip'])) ? $this->_run_mod_handler('replace', true, $_tmp, "\\", "") : smarty_modifier_replace($_tmp, "\\", "")); ?>
</td>
			<td><?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['uid']; ?>
</td>
			<td><?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['reason']; ?>
</td>
			<td><?php echo $this->_tpl_vars['webinterface']->parseDate($this->_tpl_vars['banList'][$this->_sections['ban']['index']]['created'],0,'r'); ?>
</td>
			<td><?php if ($this->_tpl_vars['banList'][$this->_sections['ban']['index']]['duration'] > 0): ?><?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['duration']; ?>
<?php else: ?><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_unlimited']; ?>
<?php endif; ?></td>
			<td><?php if ($this->_tpl_vars['banList'][$this->_sections['ban']['index']]['duration'] > 0): ?><?php echo $this->_tpl_vars['webinterface']->parseDate($this->_tpl_vars['banList'][$this->_sections['ban']['index']]['created'],$this->_tpl_vars['banList'][$this->_sections['ban']['index']]['duration'],'r'); ?>
<?php else: ?><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_banlist_never']; ?>
<?php endif; ?></td>
			<td align="center"><a href="javascript:deleteBan(<?php echo $this->_tpl_vars['banList'][$this->_sections['ban']['index']]['banid']; ?>
);"><img src="modules/TS3Admin/images/delete.png" alt="delete" border="0" /></a></td>
		</tr>
<?php endfor; endif; ?>
<?php endif; ?>
	</table>
