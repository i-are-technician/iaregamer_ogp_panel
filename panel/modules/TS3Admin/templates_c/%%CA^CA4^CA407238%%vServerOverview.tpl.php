<?php /* Smarty version 2.6.30, created on 2019-08-23 06:53:09
         compiled from vServerOverview.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'is_array', 'vServerOverview.tpl', 14, false),)), $this); ?>
<div>
	<a href="#" id="clearLink" style=""></a>
	<img src="modules/TS3Admin/images/loading.gif" id="loadingImage" style="visibility:hidden;" />
	<img src="modules/TS3Admin/images/spacer.png" width="16" height="16" style="visibility:hidden;" />
	<a href="home.php?m=TS3Admin&token"><?php echo $this->_tpl_vars['lang']['OGP_LANG_head_vserver_token']; ?>
</a> | <a href="home.php?m=TS3Admin&liveview"><?php echo $this->_tpl_vars['lang']['OGP_LANG_head_vserver_liveview']; ?>
</a>
</div>

<fieldset class="infoBox" style="width:500px;float:left;"><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_virtualserver']; ?>
 #<?php echo $this->_tpl_vars['data']['virtualserver_id']; ?>
 <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_information_head']; ?>
</legend>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td colspan="2"><div class="propHeadline"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_general_head']; ?>
</b></div></td>
	</tr>
	<?php if ($this->_tpl_vars['subusers_installed'] && $this->_tpl_vars['is_parent_user']): ?>
		<?php if (is_array($this->_tpl_vars['subusers'])): ?>
		<tr>
			<td width="120"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_assign_to_subuser']; ?>
</b></td>
			<td>
			<form method=POST>
			<input type=hidden name=assign_subuser>
			<select name="user_id" onchange="this.form.submit();">
			<option><?php echo $this->_tpl_vars['lang']['OGP_LANG_select_subuser']; ?>
</option>
			<?php $_from = $this->_tpl_vars['subusers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['subuser']):
?>
			<option value="<?php echo $this->_tpl_vars['subuser']['user_id']; ?>
"><?php echo $this->_tpl_vars['subuser']['users_login']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
			</form>
			</td>
		</tr>
		<?php endif; ?>
		<?php if (is_array($this->_tpl_vars['subusers_assigned'])): ?>
		<tr>
			<td width="120"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_unassign_from_subuser']; ?>
</b></td>
			<td>
			<form method=POST>
			<input type=hidden name=unassign_subuser>
			<select name="user_id" onchange="this.form.submit();">
			<option><?php echo $this->_tpl_vars['lang']['OGP_LANG_select_subuser']; ?>
</option>
			<?php $_from = $this->_tpl_vars['subusers_assigned']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['subuser']):
?>
			<option value="<?php echo $this->_tpl_vars['subuser']['user_id']; ?>
"><?php echo $this->_tpl_vars['subuser']['users_login']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
			</form>
			</td>
		</tr>
		<?php endif; ?>
	<?php endif; ?>
	<tr>
		<td width="120"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_servername']; ?>
</b></td>
		<td><span id="virtualserver_name"><?php echo $this->_tpl_vars['data']['virtualserver_name']; ?>
</span> <a href="javascript:serveredit('virtualserver_name');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_host']; ?>
</b></td>
		<td><?php echo $this->_tpl_vars['data']['virtualserver_platform']; ?>
 <?php echo $this->_tpl_vars['data']['virtualserver_version']; ?>
</td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_state']; ?>
</b></td>
		<td><span id="serverstatus" class="<?php echo $this->_tpl_vars['data']['virtualserver_status']; ?>
"><?php echo $this->_tpl_vars['data']['virtualserver_status']; ?>
</span><?php if ($this->_tpl_vars['data']['virtualserver_status'] == 'online'): ?> - <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_ip']; ?>
:<?php echo $this->_tpl_vars['lang']['OGP_LANG_vsselect_port']; ?>
 <a href="ts3server://<?php echo $this->_tpl_vars['display_public_ip']; ?>
:<?php echo $this->_tpl_vars['data']['virtualserver_port']; ?>
"><?php echo $this->_tpl_vars['display_public_ip']; ?>
:<?php echo $this->_tpl_vars['data']['virtualserver_port']; ?>
</a><?php endif; ?></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_uptime']; ?>
</b></td>
		<td><span id="virtualserver_uptime"><?php echo $this->_tpl_vars['webinterface']->parseTime($this->_tpl_vars['data']['virtualserver_uptime']); ?>
</span>  <a href="javascript:serverupdate(new Array('virtualserver_uptime'));" class="edit"><img src="modules/TS3Admin/images/refresh.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_welcomemsg']; ?>
</b></td>
		<td><span id="virtualserver_welcomemessage"><?php echo $this->_tpl_vars['data']['virtualserver_welcomemessage']; ?>
</span> <a href="javascript:serveredit('virtualserver_welcomemessage');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg']; ?>
</b></td>
		<td><span id="virtualserver_hostmessage"><?php echo $this->_tpl_vars['data']['virtualserver_hostmessage']; ?>
</span> <a href="javascript:serveredit('virtualserver_hostmessage');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b>&nbsp;&rArr; <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg_mode_output']; ?>
</b></td>
		<td>
			<span id="virtualserver_hostmessage_mode">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input type="radio" name="virtualserver_hostmessage_mode" value="0"<?php if ($this->_tpl_vars['data']['virtualserver_hostmessage_mode'] == 0): ?> checked="checked"<?php endif; ?> onchange="serveredit_enum('virtualserver_hostmessage_mode');" /> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg_mode_0']; ?>
</td>
						<td><input type="radio" name="virtualserver_hostmessage_mode" value="1"<?php if ($this->_tpl_vars['data']['virtualserver_hostmessage_mode'] == 1): ?> checked="checked"<?php endif; ?> onchange="serveredit_enum('virtualserver_hostmessage_mode');" /> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg_mode_1']; ?>
</td>
					</tr>
					<tr>
						<td><input type="radio" name="virtualserver_hostmessage_mode" value="2"<?php if ($this->_tpl_vars['data']['virtualserver_hostmessage_mode'] == 2): ?> checked="checked"<?php endif; ?> onchange="serveredit_enum('virtualserver_hostmessage_mode');" /> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg_mode_2']; ?>
</td>
						<td><input type="radio" name="virtualserver_hostmessage_mode" value="3"<?php if ($this->_tpl_vars['data']['virtualserver_hostmessage_mode'] == 3): ?> checked="checked"<?php endif; ?> onchange="serveredit_enum('virtualserver_hostmessage_mode');" /> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostmsg_mode_3']; ?>
</td>
					</tr>
				</table>
			</span> 
		</td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_req_security']; ?>
</b></td>
		<td><span id="virtualserver_needed_identity_security_level"><?php echo $this->_tpl_vars['data']['virtualserver_needed_identity_security_level']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_req_securitylvl']; ?>
<a href="javascript:serveredit('virtualserver_needed_identity_security_level');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	
	<tr>
		<td colspan="2"><div class="propHeadline"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostbanner_head']; ?>
</b></div></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostbanner_url']; ?>
</b></td>
		<td><span id="virtualserver_hostbanner_url"><?php echo $this->_tpl_vars['data']['virtualserver_hostbanner_url']; ?>
</span> <a href="javascript:serveredit('virtualserver_hostbanner_url');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostbanner_imgurl']; ?>
</b></td>
		<td><span id="virtualserver_hostbanner_gfx_url"><?php echo $this->_tpl_vars['data']['virtualserver_hostbanner_gfx_url']; ?>
</span> <a href="javascript:serveredit('virtualserver_hostbanner_gfx_url');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_hostbanner_buttonurl']; ?>
</b></td>
		<td><span id="virtualserver_hostbutton_url"><?php echo $this->_tpl_vars['data']['virtualserver_hostbutton_url']; ?>
</span> <a href="javascript:serveredit('virtualserver_hostbutton_url');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	
	<tr>
		<td colspan="2"><div class="propHeadline"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_head']; ?>
</b></div></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_warning']; ?>
</b></td>
		<td><span id="virtualserver_antiflood_points_needed_warning"><?php echo $this->_tpl_vars['data']['virtualserver_antiflood_points_needed_warning']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_points']; ?>
 <a href="javascript:serveredit('virtualserver_antiflood_points_needed_warning');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_kick']; ?>
</b></td>
		<td><span id="virtualserver_antiflood_points_needed_kick"><?php echo $this->_tpl_vars['data']['virtualserver_antiflood_points_needed_kick']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_points']; ?>
 <a href="javascript:serveredit('virtualserver_antiflood_points_needed_kick');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_ban']; ?>
</b></td>
		<td><span id="virtualserver_antiflood_points_needed_ban"><?php echo $this->_tpl_vars['data']['virtualserver_antiflood_points_needed_ban']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_points']; ?>
 <a href="javascript:serveredit('virtualserver_antiflood_points_needed_ban');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_banduration']; ?>
</b></td>
		<td><span id="virtualserver_antiflood_ban_time"><?php echo $this->_tpl_vars['data']['virtualserver_antiflood_ban_time']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_in_seconds']; ?>
 <a href="javascript:serveredit('virtualserver_antiflood_ban_time');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_decrease']; ?>
</b></td>
		<td><span id="virtualserver_antiflood_points_tick_reduce"><?php echo $this->_tpl_vars['data']['virtualserver_antiflood_points_tick_reduce']; ?>
</span> <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_info_antiflood_points_per_tick']; ?>
 <a href="javascript:serveredit('virtualserver_antiflood_points_tick_reduce');" class="edit"><img src="modules/TS3Admin/images/edit.png" alt="edit" border="0" /></a></td>
	</tr>
</table>
</fieldset>


<fieldset class="infoBox" style="width:300px;float:left;"><legend><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_virtualserver']; ?>
 #<?php echo $this->_tpl_vars['data']['virtualserver_id']; ?>
 <?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_connection_head']; ?>
</legend>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td colspan="3"><div class="propHeadline"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_total_head']; ?>
</b> <a href="javascript:serverupdate(new Array('connection_packets_sent_total', 'connection_packets_received_total', 'connection_bytes_sent_total', 'connection_bytes_received_total', 'connection_bandwidth_sent_last_second_total', 'connection_bandwidth_received_last_second_total', 'connection_bandwidth_sent_last_minute_total', 'connection_bandwidth_received_last_minute_total'));" class="edit"><img src="modules/TS3Admin/images/refresh.png" alt="edit" border="0" /></a></div></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><span class="small italic"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_total_send']; ?>
</span></td>
		<td><span class="small italic"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_total_received']; ?>
</span></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_total_packets']; ?>
</b></td>
		<td><span id="connection_packets_sent_total"><?php echo $this->_tpl_vars['data']['connection_packets_sent_total']; ?>
</span></td>
		<td><span id="connection_packets_received_total"><?php echo $this->_tpl_vars['data']['connection_packets_received_total']; ?>
</span></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_total_bytes']; ?>
</b></td>
		<td><span id="connection_bytes_sent_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToMB($this->_tpl_vars['data']['connection_bytes_sent_total']); ?>
</span> <span class="small">MB</span></td>
		<td><span id="connection_bytes_received_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToMB($this->_tpl_vars['data']['connection_bytes_received_total']); ?>
</span> <span class="small">MB</span></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><div class="propHeadline"><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_head']; ?>
</b></div></td>
	</tr>
	<tr>
		<td><span class="small italic"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_last']; ?>
</span></td>
		<td><span class="small italic"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_send']; ?>
</span></td>
		<td><span class="small italic"><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_received']; ?>
</span></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_second']; ?>
</b></td>
		<td><span id="connection_bandwidth_sent_last_second_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToKB($this->_tpl_vars['data']['connection_bandwidth_sent_last_second_total']); ?>
</span> <span class="small">kB</span></td>
		<td><span id="connection_bandwidth_received_last_second_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToKB($this->_tpl_vars['data']['connection_bandwidth_received_last_second_total']); ?>
</span> <span class="small">kB</span></td>
	</tr>
	<tr>
		<td><b><?php echo $this->_tpl_vars['lang']['OGP_LANG_vsoverview_conn_bandwidth_minute']; ?>
</b></td>
		<td><span id="connection_bandwidth_sent_last_minute_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToKB($this->_tpl_vars['data']['connection_bandwidth_sent_last_minute_total']); ?>
</span> <span class="small">kB/s</span></td>
		<td><span id="connection_bandwidth_received_last_minute_total"><?php echo $this->_tpl_vars['webinterface']->convertByteToKB($this->_tpl_vars['data']['connection_bandwidth_received_last_minute_total']); ?>
</span> <span class="small">kB/s</span></td>
	</tr>
</table>
</fieldset>